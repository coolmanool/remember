#! /bin/bash

pid=$(ps aux | grep uwsgi | grep $USER | head -n 1 | awk '{print $2}')
prg=$(ps aux | grep uwsgi | grep $USER | head -n 1 | awk '{print $11}')

if [ "$prg" = "grep" ]; then
    rm ~/pid/uwsgi.pid 2> /dev/null
    echo 'UWSGI is OFF'
else
    if [ -f ~/pid/uwsgi.pid ]; then
        if [ "$pid" != "$(cat ~/pid/uwsgi.pid)" ]; then
            kill -9 $pid
            echo 'UWSGI is OFF'
        else
            echo 'UWSGI is ON'
        fi
    else
        kill -9 $pid
        echo 'UWSGI is OFF'
    fi
fi
