MACH.utils = (function () {
    var __weekDaysStr = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
        __monthStr = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

    var __getCurrentDate = function () {
            var d = new Date();
            // исключаем время суток из даты, чтобы дельта между днями бралась правильно
            return new Date(d.getFullYear(), d.getMonth(), d.getDate());
        },
        __getDatesDelta = function (date1, date2) {
            var d = Math.round(Math.abs(date2.getTime() - date1.getTime())/(24*60*60*1000));
            return date2.getTime() < date1.getTime() ? -d : d;
        },
        __getDateString = function (date, excludeDay) {
            var yy = date.getFullYear(),
                mm = date.getMonth() + 1,
                dd = date.getDate(),
                s = yy + '-' + (mm < 10 ? '0' + mm : mm);

            return excludeDay ? s : s + '-' + (dd < 10 ? '0' + dd : dd);
        },
        __setStorageDate = function (date, value, timewarn) {
            window.localStorage.setItem(__getDateString(date), value.toString() + ':' + timewarn.toString());
        },
        __getStorageDate = function (date) {
            var rawInfo = window.localStorage.getItem(__getDateString(date)),
                strInfo = rawInfo && rawInfo.split(':'),
                info = strInfo && {value: Number(strInfo[0]), timewarn: Number(strInfo[1])};

            return info && info.value != MACH.conf.get('CalendarConfig', 'CELL_EMPTY_VALUE') ? info : null;
        },
        __hasStorageDate = function (date) {
            return window.localStorage.getItem(__getDateString(date)) !== null;
        },
        __getCalendarValue = function (value, type) {
            if (type === MACH.conf.get('CalendarConfig', 'CALENDAR_TYPE_2')) {
                return value > 2 ? 4 : 0;
            } else if (type === MACH.conf.get('CalendarConfig', 'CALENDAR_TYPE_3')) {
                return value < 2 ? 0 : (value < 4 ? 2 : 4);
            } else if (type === MACH.conf.get('CalendarConfig', 'CALENDAR_TYPE_5')) {
                return value;
            }
        },
        __getCalendarEditStep = function (type) {
            if (type === MACH.conf.get('CalendarConfig', 'CALENDAR_TYPE_2')) {
                return 4;
            } else if (type === MACH.conf.get('CalendarConfig', 'CALENDAR_TYPE_3')) {
                return 2;
            } else if (type === MACH.conf.get('CalendarConfig', 'CALENDAR_TYPE_5')) {
                return 1;
            }
        },
        __getCalendarEditDefaultValue = function (type) {
            if (type === MACH.conf.get('CalendarConfig', 'CALENDAR_TYPE_2')) {
                return 0;
            } else if (type === MACH.conf.get('CalendarConfig', 'CALENDAR_TYPE_3')) {
                return 2;
            } else if (type === MACH.conf.get('CalendarConfig', 'CALENDAR_TYPE_5')) {
                return 2;
            }
        },
        __toggle = function ($e, b) {
            if (b) {
                $e.show();
            } else {
                $e.hide();
            }
        },
        __toggleOpacity = function ($e, b) {
            if (b) {
                $e.css('opacity', '1');
                $e.css('cursor', 'pointer');
            } else {
                $e.css('opacity', '0');
                $e.css('cursor', 'default');
            }
        },
        __createElem = function (elem, className, innerHtml) {
            var e = document.createElement(elem);
            if (className) e.className = className;
            if (innerHtml) e.innerHTML = innerHtml;
            return e;
        },
        __createTableCellView = function (date, info, canEdit) {
            var cell = __createElem('div'),
                $cell = $(cell),

                topHolder = __createElem('span', 'td-top'),
                loadingElem = __createElem('div', 'js-loading td-top__loading'),
                editElem = __createElem('span', 'js-edit material-icons td-top__edit', 'create'),
                timewarnElem = __createElem('span', 'js-timewarn material-icons td-top__alarm', 'alarm'),

                centerHolder = __createElem('span', 'td-center'),
                dateElem = __createElem('span', 'date', date.getDate().toString()),

                bottomHolder = __createElem('span', 'td-bottom'),
                infoElem = __createElem('div', 'td-bottom__info-pic');

            $(loadingElem).hide();
            topHolder.appendChild(loadingElem);

            editElem.setAttribute('title', 'Редактировать');
            if (canEdit && __getDatesDelta(date, __getCurrentDate()) >= 0) topHolder.appendChild(editElem);

            timewarnElem.setAttribute('title', 'День отмечен несвоевременно');
            __toggle($(timewarnElem), info && info.timewarn);
            topHolder.appendChild(timewarnElem);

            centerHolder.appendChild(dateElem);

            bottomHolder.appendChild(infoElem);

            cell.appendChild(topHolder);
            cell.appendChild(centerHolder);
            cell.appendChild(bottomHolder);


            return $cell;
        },
        __createTableCellEdit = function () {
            var cell = __createElem('div'),
                $cell = $(cell),

                topHolder = __createElem('span', 'td-top'),
                timewarnElem = __createElem('span', 'js-timewarn material-icons td-top__alarm', 'alarm'),

                centerHolder = __createElem('span', 'td-center'),
                /*smileElem = __createElem('div', 'js-smile td-center__smile'),
                arrowLeftElem = __createElem('span', 'js-prev_value td-center__arrow', '&lsaquo;'),
                arrowRightElem = __createElem('span', 'js-next_value td-center__arrow', '&rsaquo;'),*/

                dropdownElem = __createElem('div'),
                optionElem = __createElem('option', 'td-center__option'),

                bottomHolder = __createElem('span', 'td-bottom'),
                confirmElem = __createElem('span', 'js-confirm material-icons td-bottom__confirm', 'check_box'),
                cancelElem = __createElem('span', 'js-cancel material-icons td-bottom__cancel', 'block');

            timewarnElem.setAttribute('title', 'Вы отмечаете день несвоевременно');

            /*optionElem.setAttribute('data-image', 'static/pic/smile/smile_00.png');
            optionElem.setAttribute('value', '0');
            optionElem.setAttribute('selected', 'selected');*/
            dropdownElem.setAttribute('id', 'js-dropdown');

            confirmElem.setAttribute('title', 'Подтвердить');
            cancelElem.setAttribute('title', 'Отмена');

            topHolder.appendChild(timewarnElem);

            /*centerHolder.appendChild(arrowLeftElem);
            centerHolder.appendChild(smileElem);
            centerHolder.appendChild(arrowRightElem);*/
            //dropdownElem.appendChild(optionElem);

            //dropdownElem.appendChild(optionElem);
            centerHolder.appendChild(dropdownElem);

            bottomHolder.appendChild(confirmElem);
            bottomHolder.appendChild(cancelElem);

            cell.appendChild(topHolder);
            cell.appendChild(centerHolder);
            cell.appendChild(bottomHolder);

            $cell.hide();
            return $cell;
        },
        __createTableCell = function (date, isShowingMonth, canEdit, calendarType) {
            date = new Date(date);

            var viewInfo = __getStorageDate(date),
                editInfo,

                td = __createElem('td', '', ''),

                $td = $(td),
                $view = __createTableCellView(date, viewInfo, canEdit),
                $edit = __createTableCellEdit(),

                editStep = __getCalendarEditStep(calendarType),
                editDefaultValue = __getCalendarEditDefaultValue(calendarType);

            function __updateCellInEditState() {
                __toggleOpacity($edit.find('.js-prev_value'), editInfo.value > MACH.conf.get('CalendarConfig', 'CELL_EDIT_MIN_VALUE'));
                __toggleOpacity($edit.find('.js-next_value'), editInfo.value < MACH.conf.get('CalendarConfig', 'CELL_EDIT_MAX_VALUE'));
                //$edit.find('.js-smile').attr('src', 'static/pic/smile_' + editInfo.value.toString() + '.png');
                $edit.find('.js-smile').attr('title', '');
                td.setAttribute('data-value', editInfo.value);
            }

            if (!isShowingMonth) {
                $td.addClass('other-month');
            }
            td.setAttribute('data-value', viewInfo ? viewInfo.value : '');

            $view.find('.js-edit').on('click', function () {    /*background-repeat: round;*/
                var delta = __getDatesDelta(date, __getCurrentDate());
                if (delta >= 0) {
                    editInfo = {
                        value: viewInfo ? viewInfo.value : editDefaultValue,
                        timewarn: Number(delta >= MACH.conf.get('CalendarConfig', 'WARN_UPLOAD_DAYS_DELTA_CLIENT'))
                    };

                    __toggle($edit.find('.js-timewarn'), editInfo.timewarn);
                    __updateCellInEditState();

                    $view.hide();
                    $edit.show();
                }
            });

            $edit.find('.js-prev_value').on('click', function () {
                editInfo.value = Math.max(editInfo.value - editStep, MACH.conf.get('CalendarConfig', 'CELL_EDIT_MIN_VALUE'));
                __updateCellInEditState();
            });
            $edit.find('.js-next_value').on('click', function () {
                editInfo.value = Math.min(editInfo.value + editStep, MACH.conf.get('CalendarConfig', 'CELL_EDIT_MAX_VALUE'));
                __updateCellInEditState();
            });
            $edit.find('.js-confirm').on('click', function () {
                $view.find('.js-edit').hide();
                $view.find('.js-loading').show();

                $edit.hide();
                $view.show();

                var form = MACH.forms.get('UploadCalendarForm');
                form.setFieldValue('date', __getDateString(date));
                form.setFieldValue('value', editInfo.value);
                form.setFieldValue('timewarn', editInfo.timewarn);
                form.send(function () {
                    __setStorageDate(date, editInfo.value, editInfo.timewarn);

                    viewInfo = editInfo;
                    __toggle($view.find('.js-timewarn'), viewInfo.timewarn);

                    $view.find('.js-loading').hide();
                    $view.find('.js-edit').show();
                });
            });
            $edit.find('.js-cancel').on('click', function () {
                $edit.hide();
                $view.show();

                td.setAttribute('data-value', viewInfo ? viewInfo.value : '');
            });

            $td.append($view);
            $td.append($edit);

            return td;
        },
        _clearStorage = function () {
            window.localStorage.clear();
        },
        _pushPageState = function (ownerId, date, replace) {
            var state = {
                    ownerId: ownerId,
                    date: new Date(date)
                },
                url = 'id' + ownerId + '#' + __getDateString(date, true);

            if (replace) {
                window.history.replaceState(state, '', url);
            } else {
                window.history.pushState(state, '', url);
            }
        },
        _onPopPageState = function (calendar) {
            window.addEventListener('popstate', function (e) {
                if (e.state) {
                    calendar.update(e.state.ownerId, e.state.date);
                    calendar.show();
                }
            }, false);
        },
        _onPageHashChange = function (calendar) {
            $(window).on('hashchange', function () {
                var date = _getPageDate();
                if (calendar.date !== date) {
                    calendar.update(null, date);
                    calendar.show('replace');
                }
            });
        },
        _getPageDate = function () {
            var d = new Date(location.hash.substr(1));
            if (isNaN(d.getTime())) {
                d = __getCurrentDate();
            }
            // возвращаем 1 число, чтобы при увеличении/уменьшении месяца не перескакивать через один
            // ex: 2016-01-30 + 1 month -> 2016-03-01
            return new Date(d.getFullYear(), d.getMonth());
        },
        _getShowDateBegin = function (date) {
            var fromDate = new Date(date.getFullYear(), date.getMonth());
            var weekDay = fromDate.getDay() === 0 ? 7 : fromDate.getDay();
            if (weekDay == 1) {
                fromDate.setDate(fromDate.getDate() - 7);
            } else {
                fromDate.setDate(fromDate.getDate() - weekDay + 1);
            }
            return fromDate;
        },
        _getDownloadDateBegin = function (fromDate, counter) {
            var date = new Date(fromDate);
            for (var i = 0; i < counter; ++i) {
                if (!__hasStorageDate(date)) {
                    break;
                }
                date.setDate(date.getDate() + 1);
            }
            return i != counter ? date : null;
        },
        _getDownloadDateEnd = function (fromDate, counter) {
            var date = new Date(fromDate);
            date.setDate(date.getDate() + counter);
            for (var i = 0; i < counter; ++i) {
                date.setDate(date.getDate() - 1);
                if (!__hasStorageDate(date)) {
                    break;
                }
            }
            return i != counter ? date : null;
        },
        _createCalendarTable = function (showDate, fromDate, width, height, canEdit, type) {
            var date = new Date(fromDate),
                table = document.createElement('table'),
                tr = document.createElement('tr');

            for (var i = 0, td; i < width; ++i) {
                td = __createElem('td', '', __weekDaysStr[i]);
                tr.appendChild(td);
            }
            table.appendChild(tr);

            for (i = 0; i < height; ++i) {
                tr = document.createElement('tr');
                for (var j = 0; j < width; ++j) {
                    tr.appendChild(__createTableCell(date, date.getMonth() === showDate.getMonth(), canEdit, type));
                    date.setDate(date.getDate() + 1);
                }
                table.appendChild(tr);
            }

            var $table = $(table);

            /*var iconSelect;
            iconSelect = new IconSelect("js-dropdown");

            var icons = [];
            icons.push({'iconFilePath':'/static/pic/smile/smile_00.png', 'iconValue':'1'});
            icons.push({'iconFilePath':'/static/pic/smile/smile_02.png', 'iconValue':'2'});
            icons.push({'iconFilePath':'/static/pic/smile/smile_04.png', 'iconValue':'3'});

            iconSelect.refresh(icons);*/

            return $table;
        },
        _getCalendarTitle = function (date) {
            return __monthStr[date.getMonth()] + ' ' + date.getFullYear();
        },
        _downloadCalendar = function (ownerId, dateBegin, dateEnd, calendarType, callback) {
            var form = MACH.forms.get('DownloadCalendarForm');
            form.setFieldValue('owner_id', ownerId);
            form.setFieldValue('date_from', __getDateString(dateBegin));
            form.setFieldValue('date_to', __getDateString(dateEnd));
            form.send(function (data) {
                for (var date = new Date(dateBegin), info; date <= dateEnd; date.setDate(date.getDate() + 1)) {
                    info = data[__getDateString(date)];
                    if (info) {
                        __setStorageDate(date, __getCalendarValue(info[0], calendarType), info[1]);
                    } else {
                        __setStorageDate(date, MACH.conf.get('CalendarConfig', 'CELL_EMPTY_VALUE'), 0);
                    }
                }

                if (callback) {
                    callback();
                }
            });
        },
        _userSignin = function () {
            VK.Auth.login(function (r) {
                var form = MACH.forms.get('SigninVkForm'),
                    name = (r.session.user.first_name + ' ' + r.session.user.last_name).substr(0, form.getFieldOption('name', 'maxlength'));

                form
                    .setFieldValue('expire', r.session.expire)
                    .setFieldValue('mid', r.session.mid)
                    .setFieldValue('secret', r.session.secret)
                    .setFieldValue('sid', r.session.sid)
                    .setFieldValue('sig', r.session.sig)
                    .setFieldValue('name', name);
                form.send(function(){alert('жопа');});
            });
        },
        _userSignout = function () {
            var form = MACH.forms.get('SignoutForm');
            form.send();
        };
    return {
        clearStorage: _clearStorage,
        pushPageState: _pushPageState,
        onPopPageState: _onPopPageState,
        onPageHashChange: _onPageHashChange,

        getPageDate: _getPageDate,
        getShowDateBegin: _getShowDateBegin,

        getDownloadDateBegin: _getDownloadDateBegin,
        getDownloadDateEnd: _getDownloadDateEnd,

        getCalendarTitle: _getCalendarTitle,

        createCalendarTable: _createCalendarTable,
        downloadCalendar: _downloadCalendar,

        userSignin: _userSignin,
        userSignout: _userSignout
    };
})();