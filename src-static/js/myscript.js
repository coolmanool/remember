function __createElem(elem, className, innerHtml) {
    var e = document.createElement(elem);
    if (className) e.className = className;
    if (innerHtml) e.innerHTML = innerHtml;
    return e;
}

function __createLi(elem, ulElem, flag){
    if  (elem.length < 1) {
        var liElem = __createElem('li', 'li-elems-none', 'По данной комбинации цифр никаких слов не найдено. ' +
                                                            'Попробуйте поменять запрос');
        ulElem.appendChild(liElem);
    } else {
        elem.forEach(function (item, k, elem) {
            var wordElem = __createElem('div', 'word', item.w);

            var liElem;

            if (flag) {
                if (item.i>0){
                    liElem = __createElem('li', 'li-word li-one-word li-ok-word');
                } else {
                    liElem = __createElem('li', 'li-word li-one-word li-added-word');
                }
                liElem.appendChild(wordElem);
                var thumbUpElem = __createElem('i', 'material-icons icon-thumb icon-thumb-up js-thumb-up', 'thumb_up'),
                    thumbDownElem = __createElem('i', 'material-icons icon-thumb icon-thumb-down js-thumb-down', 'thumb_down'),
                    saveElem = __createElem('i', 'material-icons icon-thumb icon-save js-btn-save', 'add');
                if ($('li').is('.logged')) {
                    liElem.appendChild(thumbUpElem);
                    liElem. appendChild(thumbDownElem);
                    liElem.appendChild(saveElem);
                }
            } else {
                liElem = __createElem('li', 'li-word li-some-word');
                liElem.appendChild(wordElem);
            }
            liElem.setAttribute('data-id', item.i);

            ulElem.appendChild(liElem);
        });
    }
}

function __manageData(data, $words) {
    var flag = 1;

    if (data.numbers.length > 1) {
        flag = 0;
    }

    for (var i = 0; i < data.numbers.length; ++i) {
        var divElem = __createElem('div', 'b-words'),
            ulElem = __createElem('ul', 'ul-words'),
            numElem = __createElem('div', 'num-out', data.numbers[i]);

        divElem.appendChild(numElem);

        __createLi(data.words[i], ulElem, flag);

        divElem.appendChild(ulElem);
        $words.append(divElem);
    }
}

function __getWords() {
    var $inputnum = $('.js-num');
    if ($inputnum.val() == ($inputnum.val()).match(/[0-9-]+/)) {
        $('.js-translate')
            .off('click')
            .addClass('disabled');

        var $words = $('.js-words');
        $words.empty();
        var form = MACH.forms.get('GetWordsForm');
        form.setFieldValue('input_string', $inputnum.val());
        form.setFieldValue('auto_split', $('.js-auto-split').is(':checked'));
        form.setFieldValue('dop_words', $('.js-dop-words').is(':checked'));
        form.setFieldValue('ext_words', $('.js-ext-words').is(':checked'));
        form.setFieldValue('added_words', $('.js-added-words').is(':checked'));
        form.send(function(data) {
            $inputnum.val(data.numbers.join('-'));

            $('.btns-like')
                .addClass('hide');

            $('.js-like-msg').empty();

            $('.btn-good-phrase')
                .off('click');

            $('.btn-bad-phrase')
                .off('click');

            $('.btn-save-query')
                .off('click');

            __manageData(data, $words);

            $('.li-some-word')
                    .off('click')
                    .on('click', __checkLi);

            $('.js-translate')
                    .on('click', __getWords)
                    .removeClass('disabled');

            $('.js-thumb-up').on('click', function() {
                __like($(this).parent('.li-word'), true);
            });

            $('.js-thumb-down').on('click', function() {
                __like($(this).parent('.li-word'), false);
            });

            $('.js-btn-save').on('click', function() {
                __saveQuery($(this).parent('.li-word').children('.word'));
            });
        });
    } else {
        alert('В этом поле допускается ввод только цифр и дефисов!');
    }
    return false;
}

/*******************************************/
/*********GET IDS OR JUST STRING************/
/*******************************************/
function __getString($elems, attr) {
    var string = "";

    if (attr=='data-id') {
        $elems.each(function(i){
            string += ',' + $(this).attr(attr);
        });
    } else {
        $elems.each(function(i){
            string += ' ' + $(this).text();
        });
    }
    string = string.substr(1);
    return string;
}


function __like($elems, flaglike) {
    MACH.forms
            .get('SetWeightForm')
            .setFieldValue('input_string', __getString($elems, 'data-id'))
            .setFieldValue('flaglike', flaglike)
            .send(function(data) {
            var msg = $('.js-like-msg');
            if (flaglike === true) {
                msg
                    .html('Выбранная фраза отмечена, как удачная!')
                    .removeClass('msg-error')
                    .removeClass('msg-save')
                    .addClass('msg-success');
            } else {
                msg
                    .html('Выбранная фраза отмечена, как неудачная!')
                    .addClass('msg-error')
                    .removeClass('msg-success')
                    .removeClass('msg-save');
            }
            });
}

function __checkLi() {
    var $this = $(this);
    $this.parent('.ul-words').find('.li-check').removeClass('li-check');
    $this.addClass('li-check');
    if ($('.li-check').length == $('.ul-words').length) {
        $('.btns-like').removeClass('hide');

        $('.btn-good-phrase').on('click', function() {
            __like($('.li-check'), true);
        });

        $('.btn-bad-phrase').on('click', function(){
            __like($('.li-check'), false);
        });

        $('.btn-save-query')
            .off('click')
            .on('click', function() {
            __saveQuery($('.li-check .word'));
        });
    }
}

function __addWord() {
    var $newword = $('.js-new-word');

    if ($newword.val() == ($newword.val()).match(/[a-яА-ЯЁё-]+/)){
        var form = MACH.forms.get('AddWordForm'),
            $msg = $('.msg');
        form.setFieldValue('word', $newword.val());
        form.send(function(data) {
            console.log(data);
            $msg
                .html('Слово успешно добавлено!')
                .addClass('msg-success')
                .removeClass('msg-error');
            document.location.reload();
        }, function (data) {
            $msg
                .addClass('msg-error')
                .removeClass('msg-success');
            if (data==='profanity') {
                $msg.html('Это слово входит в список запрещенных слов в этой системе!');
            } else if (data==='empty') {
                $msg.html('Извините, заданное вами слово не удалось преобразовать ни в какую комбинацию цифр:(');
            } else if (data==='exists') {
                $msg.html('Заданное вами слово уже существует в системе!');
            }
        });
    } else {
        alert('Слово может состоять только из прописных или строчных русских букв и дефисов!');
    }
}

function __saveQuery($elems) {
    var form = MACH.forms.get('SaveQueryForm');
    form.setFieldValue('words', __getString($elems));
    form.setFieldValue('query', $('.js-num').val());
    form.send(function(data){
        $('.js-like-msg')
            .html('Слова успешно сохранены!')
            .addClass('msg-save')
            .removeClass('msg-error')
            .removeClass('msg-success');
    });
}

function __deleteQuery() {
    var form = MACH.forms.get('DeleteQueryForm');
    form.setFieldValue('id', $(this).attr('data-id'));
    form.send(function(data){
        document.location.reload();
    });
}

function __likeSavedPhrase() {
    var form = MACH.forms.get('LikeSavedPhraseForm');
    form.setFieldValue('phrase_owner', $(this).attr('data-owner'));
    form.setFieldValue('phrase_id', $(this).attr('data-id'));
    form.send(function(data){
        document.location.reload();
    });
}