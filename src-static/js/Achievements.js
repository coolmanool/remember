MACH.Achievements = function ($holder, viewerId, ownerId) {
    this.$holder = $holder;
    this.viewerId = viewerId;
    this.ownerId = ownerId;
    this.seq = null;
};

MACH.Achievements.prototype.show = function (forward) {
    var self = this;

    function __createLoadingElem() {
        var elem = document.createElement('div');
        elem.className = 'table__loading';
        return elem;
    }

    MACH.forms
        .get('GetAchievementsForm')
        .setFieldValue('seq', self.seq)
        .setFieldValue('forward', forward)
        .setFieldValue('owner_id', self.ownerId)
        .send(function (data) {
            self.seq = data.seq;
            // create table
        });

    self.$holder.html(__createLoadingElem());
};
