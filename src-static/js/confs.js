MACH.confs = (function () {
    var __configs = {};

    var _set = function (confName, propName, propValue) {
            var conf = __configs[confName];
            if (conf === undefined) {
                conf = {};
                __configs[confName] = conf;
            }

            conf[propName] = propValue;
        },
        _get = function (confName, propName) {
            var prop = __configs[confName][propName];
            if (prop === undefined) {
                throw new Error('No such prop: ' + confName + '.' + propName);
            }

            return prop;
        };

    return {
        set: _set,
        get: _get
    };
})();