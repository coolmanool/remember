MACH.forms = (function() {
    var __forms = {};

    var _set = function (name, form) {
            __forms[name] = form;
        },
        _get = function (name) {
            return __forms[name];
        };

    return {
        set: _set,
        get: _get
    };
})();