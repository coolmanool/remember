MACH.Form = function ($form, url) {
    this.$form = $form;
    this.url = url;
};

MACH.Form.prototype.setFieldValue = function (field, value) {
    this.$form.find('[name=' + field + ']').val(value);
    return this;
};

MACH.Form.prototype.getFieldOption = function (field, option) {
    return this.$form.find('[name=' + field + ']').attr(option);
};

MACH.Form.prototype.send = function (success, error) {
    $.ajax({
        method: 'POST',
        url: this.url,
        data: this.$form.serialize(),
        success: function (data) {
            if (data.code == MACH.confs.get('AjaxResponseConfig', 'AJAX_RESPONSE_CODE_SUCCESS')) {
                if (success) {
                    success(data.data);
                }
            } else if (data.code == MACH.confs.get('AjaxResponseConfig', 'AJAX_RESPONSE_CODE_REDIRECT')) {
                window.location.href = data.data;
            } else if (data.code == MACH.confs.get('AjaxResponseConfig', 'AJAX_RESPONSE_CODE_ERROR')) {
                if (error) {
                    error(data.data);
                } else {
                    console.log('ajax form error: ' + data.data.toString());
                }
            } else if (data.code == MACH.confs.get('AjaxResponseConfig', 'AJAX_RESPONSE_CODE_RELOAD')) {
                document.location.reload();
            }
        }
    });
};