import redis
from src.conf.config import RedisConfig

from src.db.model_words_links_weight import WordsLinksWeight


class DBCacheWordsLinks(object):
    def __init__(self):
        self.__redis = redis.Redis(unix_socket_path=RedisConfig.UNIX_SOCKET_PATH, db=RedisConfig.DB_CACHE_WORDS_LINKS_DBNUM)

    def get_weight(self, word_left, word_right):
        key = '{}_{}'.format(word_left.id, word_right.id)
        value = self.__redis.get(key)
        if value is None:
            try:
                value = WordsLinksWeight.objects.get(word1=word_left, word2=word_right).weight
            except WordsLinksWeight.DoesNotExist:
                value = 0
            self.__redis.set(key, value, RedisConfig.DB_CACHE_WORDS_LINKS_EXPIRE)
            print('Object for class ModelWordsLinksWeight args {} loaded from database'.format(key))
            return value
        else:
            self.__redis.expire(key, RedisConfig.DB_CACHE_WORDS_LINKS_EXPIRE)
            print('Object for class ModelWordsLinksWeight args {} loaded from cache'.format(key))
            return int(value)

    def set_weight(self, word_left, word_right, weight):
        key = '{}_{}'.format(word_left.id, word_right.id)
        self.__redis.set(key, weight, RedisConfig.DB_CACHE_WORDS_LINKS_EXPIRE)
