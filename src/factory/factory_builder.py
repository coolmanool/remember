from .url_builder import URLBuilder
from .session_storage import SessionStorage
from .db_cache import DBCache
from .db_cache_words_links import DBCacheWordsLinks


class FactoryBuilder(object):
    def __init__(self):
        self.URLBuilder = URLBuilder()

        self.SessionStorage = SessionStorage()
        self.DBCache = DBCache()
        self.DBCacheWordsLinks = DBCacheWordsLinks()
