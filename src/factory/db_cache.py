import redis
from urllib.parse import urlencode

from django.core import serializers

from src.conf.config import RedisConfig


# noinspection PyMethodMayBeStatic
class DBCache(object):
    def __init__(self):
        self.__redis = redis.Redis(unix_socket_path=RedisConfig.UNIX_SOCKET_PATH, db=RedisConfig.DB_CACHE_DBNUM)

    def get_object(self, cls, args, expire):
        key = self._build_key(cls, args)
        obj_json = self.__redis.get(key)
        if obj_json is None:
            print('Object for class {} args {} loaded from database'.format(cls, args))
            obj = cls.objects.get(**args)
            self.__redis.set(key, serializers.serialize('json', [obj]), expire)
        else:
            print('Object for class {} args {} loaded from cache'.format(cls, args))
            obj = next(serializers.deserialize('json', obj_json)).object
            self.__redis.expire(key, expire)
        return obj

    def update_object(self, obj, args, expire, force=False):
        key = self._build_key(obj.__class__, args)
        if force or self.__redis.exists(key):
            self.__redis.set(key, serializers.serialize('json', [obj]), expire)

    def _build_key(self, cls, args):
        return '{module}.{cls}.{instance}'.format(
            module=cls.__module__,
            cls=cls.__name__,
            instance=urlencode([x for x in sorted(args.items())])
        )
