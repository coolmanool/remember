# noinspection PyMethodMayBeStatic
class URLBuilder(object):
    def build_main_page_pattern(self):
        return r'^$'

    def build_main_page_url(self):
        return '/'

    def build_hello_page_pattern(self):
        return r'^hello$'

    def build_hello_page_url(self):
        return '/hello'

    def build_user_page_pattern(self):
        return r'^id([0-9]+)$'

    def build_user_page_url(self, user_id):
        return '/id{}'.format(user_id)

    def build_statistics_page_pattern(self):
        return r'^statistics$'

    def build_statistics_page_url(self):
        return '/statistics'

    # --- folder urls ---
    def build_ajax_folder_pattern(self):
        return r'^ajax/'

    # --- ajax names ---
    def build_ajax_signin_vk_name(self):
        return 'ajax_signin_vk'

    def build_ajax_signout_name(self):
        return 'ajax_signout'

    def build_ajax_get_words_name(self):
        return 'ajax_get_words'

    def build_ajax_set_weight_name(self):
        return 'ajax_set_weight'

    def build_ajax_add_word_name(self):
        return 'ajax_add_word'

    def build_ajax_save_query_name(self):
        return 'ajax_save_query'

    def build_ajax_delete_query_name(self):
        return 'ajax_delete_query'

    def build_ajax_like_saved_phrase_name(self):
        return 'ajax_like_saved_phrase'