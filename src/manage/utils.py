import os

from src.manage.config import DeployDevConfig


def system(cmd):
    os.system(cmd.replace('\n', ''))


def ssh(user, host, cmd):
    system("ssh {user}@{host} '{cmd}'".format(user=user, host=host, cmd=cmd))


def rsync(mode, paths, inc=None, exc=None):
    cmd = 'rsync -{}'.format(mode)

    for i in inc or []:
        cmd += " --include '{}'".format(i)

    for i in exc or []:
        cmd += " --exclude '{}'".format(i)

    cmd += ' {}'.format(paths)

    system(cmd)
