from src.db.model_word import Word
from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        with open('src/manage/management/commands/dict.txt') as file:
            for line in file:
                data = line.split()
                Word.objects.create(word=data[3], num_mix=data[0][1:], num_base=data[1][1:], num_dop=data[2][1:])