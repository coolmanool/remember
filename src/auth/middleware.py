import traceback
from src.conf.config import MainConfig, RedisConfig
from src.factory import Factory
from src.db.model_user import User, AnonymousUser

from .auth import Session


# noinspection PyMethodMayBeStatic
class AuthMiddleware(object):
    def process_request(self, request):
        user = None
        try:
            if Factory.SessionStorage.validate_session(request.COOKIES['uid'], request.COOKIES['sid']):
                user = Factory.DBCache.get_object(
                    User,
                    {'id': request.COOKIES['uid']},
                    RedisConfig.DB_CACHE_USER_EXPIRE
                )
        except KeyError as e:
            traceback.print_exception(type(e), e, None)
        except User.DoesNotExist as e:
            traceback.print_exception(type(e), e, None)
            # странная ситуация - юзер был аутентифицирован, но в базе его нет. Нужно залогировать
        except BaseException as e:
            traceback.print_exception(type(e), e, None)

        request.user = AnonymousUser() if user is None else user
        request.session = Session()

    def process_response(self, request, response):
        if request.session.is_signin():
            response.set_cookie('uid', request.COOKIES['uid'], MainConfig.AUTH_COOKIE_MAX_AGE)
            response.set_cookie('sid', request.COOKIES['sid'], MainConfig.AUTH_COOKIE_MAX_AGE)
        elif request.session.is_signout():
            response.delete_cookie('uid')
            response.delete_cookie('sid')

        return response
