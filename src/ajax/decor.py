from src.factory import Factory

from .utils import redirect


"""
    USAGE:
        @decor_authenticated
        @decor_valid_form(UploadCalendarForm)
"""


def decor_authenticated(fn):
    def wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect(Factory.URLBuilder.build_main_page_url())
        return fn(request, *args, **kwargs)
    return wrapper


def decor_valid_form(form_class):
    def decor(fn):
        def wrapper(request, *args, **kwargs):
            form = form_class(data=request.POST or None)
            if not form.is_valid():
                return form.render_errors_to_json()
            return fn(request, form, *args, **kwargs)
        return wrapper
    return decor
