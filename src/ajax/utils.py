import json

from django.http import HttpResponse

from src.conf.config import AjaxResponseConfig


def json_response(data=None, code=None):
    json_data = {
        'code': code if code is not None else AjaxResponseConfig.AJAX_RESPONSE_CODE_SUCCESS,
        'data': data if data is not None else {}
    }
    return HttpResponse(json.dumps(json_data, separators=(',', ':')), content_type="application/json")


def redirect(url):
    return json_response(url, AjaxResponseConfig.AJAX_RESPONSE_CODE_REDIRECT)


def reload():
    return json_response(code=AjaxResponseConfig.AJAX_RESPONSE_CODE_RELOAD)