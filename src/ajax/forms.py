from django import forms

from src.conf.config import AjaxResponseConfig
from src.main import form
from src.factory import Factory

from .form_fields import BooleanField, IntegerField
from .utils import json_response


class AjaxFormsHolder(object):
    forms = []


def ajax_form(cls):
    AjaxFormsHolder.forms.append(cls())
    return cls


class AjaxForm(form.Form):
    def render_errors_to_json(self):
        return json_response({e: self.errors[e][0] for e in self.errors}, AjaxResponseConfig.AJAX_RESPONSE_CODE_ERROR)


class AjaxEmptyForm(AjaxForm):
    idle = BooleanField()


@ajax_form
class SigninVkForm(AjaxForm):
    # signin fields
    expire = forms.CharField()
    mid = forms.IntegerField()
    secret = forms.CharField()
    sid = forms.CharField()
    sig = forms.CharField()

    # signup fields
    name = forms.CharField(max_length=50)

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_signin_vk_name(), **kwargs)


@ajax_form
class SignoutForm(AjaxEmptyForm):
    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_signout_name(), **kwargs)


@ajax_form
class GetWordsForm(AjaxForm):
    input_string = forms.CharField()
    auto_split = forms.BooleanField(required=False)
    dop_words = forms.BooleanField(required=False)
    ext_words = forms.BooleanField(required=False)
    added_words = forms.BooleanField(required=False)

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_get_words_name(), **kwargs)


@ajax_form
class SetWeightForm(AjaxForm):
    input_string = forms.CharField()
    flaglike = forms.BooleanField(required=False)

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_set_weight_name(), **kwargs)


@ajax_form
class AddWordForm(AjaxForm):
    word = forms.CharField()

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_add_word_name(), **kwargs)


@ajax_form
class SaveQueryForm(AjaxForm):
    words = forms.CharField()
    query = forms.CharField()

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_save_query_name(), **kwargs)


@ajax_form
class DeleteQueryForm(AjaxForm):
    id = forms.IntegerField()

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_delete_query_name(), **kwargs)


@ajax_form
class LikeSavedPhraseForm(AjaxForm):
    phrase_owner = forms.IntegerField()
    phrase_id = forms.CharField()

    def __init__(self, **kwargs):
        super().__init__(Factory.URLBuilder.build_ajax_like_saved_phrase_name(), **kwargs)