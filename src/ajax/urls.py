from django.conf.urls import url

from src.factory import Factory

from . import views


urlpatterns = [
    url(r'^signin_vk$', views.signin, name=Factory.URLBuilder.build_ajax_signin_vk_name()),
    url(r'^signout$', views.signout, name=Factory.URLBuilder.build_ajax_signout_name()),
    url(r'^get_words$', views.get_words, name=Factory.URLBuilder.build_ajax_get_words_name()),
    url(r'^set_weight$', views.set_weight, name=Factory.URLBuilder.build_ajax_set_weight_name()),
    url(r'^add_word$', views.add_word, name=Factory.URLBuilder.build_ajax_add_word_name()),
    url(r'^save_query$', views.save_query, name=Factory.URLBuilder.build_ajax_save_query_name()),
    url(r'^delete_query$', views.delete_query, name=Factory.URLBuilder.build_ajax_delete_query_name()),
    url(r'^like_saved_phrase$', views.like_saved_phrase, name=Factory.URLBuilder.build_ajax_like_saved_phrase_name())
]
