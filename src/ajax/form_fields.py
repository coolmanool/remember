from django import forms
from .form_widgets import CheckboxInput


# noinspection PyAbstractClass
class DateField(forms.DateField):
    def __init__(self, input_formats=None, *args, **kwargs):
        if input_formats is None:
            input_formats = ('%Y-%m-%d',)
        super().__init__(input_formats, *args, **kwargs)


class BooleanField(forms.BooleanField):
    def __init__(self, **kwargs):
        kwargs.setdefault('widget', CheckboxInput)
        kwargs.setdefault('required', False)
        super().__init__(**kwargs)


class IntegerField(forms.IntegerField):
    def __init__(self, **kwargs):
        kwargs.setdefault('required', False)
        super().__init__(**kwargs)
