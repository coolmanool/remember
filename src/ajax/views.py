import traceback
import itertools

from Crypto.Hash import MD5
from django.db import IntegrityError, Error as DjDBError
from django.db.models import F
from datetime import datetime, date, time

from src.conf import djconfig
from src.auth import auth
from src.conf.config import VkConfig, AjaxResponseConfig
from src.db.models import User, Word, WordsLinksWeight, Profanity, History, AddedWords, LikedPhrases, Statistics
from src.db.utils import get_words_order_by_weight, WordToNumberConverter, get_words_auto_split, statistics
from src.factory import Factory

from .decor import decor_authenticated, decor_valid_form
from .forms import SigninVkForm, SignoutForm, GetWordsForm, SetWeightForm, AddWordForm, SaveQueryForm, DeleteQueryForm, LikeSavedPhraseForm
from .utils import redirect, json_response, reload


@decor_valid_form(SigninVkForm)
def signin(request, form):
    sig = 'expire={expire}mid={mid}secret={secret}sid={sid}{app_secret}'.format(
        expire=form.cleaned_data['expire'],
        mid=form.cleaned_data['mid'],
        secret=form.cleaned_data['secret'],
        sid=form.cleaned_data['sid'],
        app_secret=VkConfig.SITE_APP_SECRET
    )
    sig = MD5.new(bytes(sig, 'utf-8')).hexdigest()

    if sig != form.cleaned_data['sig']:
        form.add_error(None, 'invalid form')
        return form.render_errors_to_json()

    try:
        user = User.objects.get(vk_id=form.cleaned_data['mid'])
    except User.DoesNotExist:
        try:
            user = User.objects.create(name=form.cleaned_data['name'], vk_id=form.cleaned_data['mid'])
        except DjDBError:
            form.add_error(None, 'invalid form')
            return form.render_errors_to_json()

    auth.signin(request, user)

    statistics('count_signin')

    return redirect(Factory.URLBuilder.build_user_page_url(user.id))


@decor_authenticated
@decor_valid_form(SignoutForm)
def signout(request, form):
    user_id = request.user.id
    auth.signout(request)
    return reload()


@decor_valid_form(GetWordsForm)
def get_words(request, form):
    statistics('count_queries')

    if form.cleaned_data['ext_words']:
        words_type = 'num_mix'
    elif form.cleaned_data['dop_words']:
        words_type = 'num_dop'
    else:
        words_type = 'num_base'

    if form.cleaned_data['auto_split']:
        numbers, words = get_words_auto_split(
            form.cleaned_data['input_string'].replace('-', ''),
            words_type,
            form.cleaned_data['added_words'],
            min_words_count=2,
            retry_count=3
        )
        if numbers is None:
            return json_response(code=AjaxResponseConfig.AJAX_RESPONSE_CODE_ERROR)
    else:
        numbers = form.cleaned_data['input_string'].split('-')
        words = get_words_order_by_weight(numbers, words_type, form.cleaned_data['added_words'])

    return json_response({
        'numbers': numbers,
        'words': [[w.to_json() for w in ws] for ws in words]
    })


@decor_authenticated
@decor_valid_form(SetWeightForm)
def set_weight(request, form):
    words_ids = [int(w) for w in form.cleaned_data['input_string'].split(',')]
    weight = 1 if form.cleaned_data['flaglike'] else -1

    for wid in words_ids:
        if wid > 0:
            Word.objects.filter(id=wid).update(weight=F('weight')+weight)
        else:
            AddedWords.objects.filter(id=-wid).update(weight=F('weight')+weight)

    a_it, b_it = itertools.tee(words_ids)
    next(b_it, None)
    for a, b in zip(a_it, b_it):
        if a > 0 and b > 0:
            try:
                WordsLinksWeight.objects.create(word1_id=a, word2_id=b, weight=weight)
            except IntegrityError:
                WordsLinksWeight.objects\
                    .filter(word1_id=a, word2_id=b)\
                    .update(weight=F('weight')+weight)

    return json_response()


@decor_authenticated
@decor_valid_form(AddWordForm)
def add_word(request, form):
    word = form.cleaned_data['word']
    word = word.lower()

    if Word.objects.filter(word=word).exists() or AddedWords.objects.filter(word=word).exists():
        return json_response(data="exists", code=AjaxResponseConfig.AJAX_RESPONSE_CODE_ERROR)
    elif Profanity.objects.filter(word=word).exists():
        return json_response(data="profanity", code=AjaxResponseConfig.AJAX_RESPONSE_CODE_ERROR)
    else:
        base, mix, dop = WordToNumberConverter.convert(word)

        if base or mix or dop:
            AddedWords.objects\
                .create(user=request.user, word=word, num_base=base, num_mix=mix, num_dop=dop, date=datetime.today())
            statistics('count_add')
            return json_response()
        else:
            return json_response(data="empty", code=AjaxResponseConfig.AJAX_RESPONSE_CODE_ERROR)


@decor_authenticated
@decor_valid_form(SaveQueryForm)
def save_query(request, form):
    user = request.user
    query = form.cleaned_data['query']
    words = form.cleaned_data['words']

    try:
        History.objects.create(user=user, query=query, words=words)
    except IntegrityError:
        return json_response(code=AjaxResponseConfig.AJAX_RESPONSE_CODE_ERROR)

    return json_response()


@decor_authenticated
@decor_valid_form(DeleteQueryForm)
def delete_query(request, form):
    try:
        obj = History.objects.get(id=form.cleaned_data['id'])
        if obj.user.id == request.user.id:
            obj.delete()
    except History.DoesNotExist:
        pass

    return json_response()


@decor_authenticated
@decor_valid_form(LikeSavedPhraseForm)
def like_saved_phrase(request, form):
    phrase_id = form.cleaned_data['phrase_id']
    owner_id = int(form.cleaned_data['phrase_owner'])

    if owner_id != request.user.id:
        if LikedPhrases.objects.filter(like_owner=request.user, phrase_id=phrase_id).exists():
            LikedPhrases.objects.filter(like_owner=request.user, phrase_id=phrase_id).delete()
            History.objects.filter(id=phrase_id).update(likes=F('likes')-1)
        else:
            LikedPhrases.objects.create(phrase_owner_id=owner_id, like_owner=request.user, phrase_id=phrase_id)
            History.objects.filter(id=phrase_id).update(likes=F('likes')+1)

    return json_response()