from src.factory import Factory
from src.conf.core import ConfigsHolder
from src.ajax.forms import AjaxFormsHolder

from src.db.model_liked_phrases import LikedPhrases
from src.db.model_history import History


def build_context():
    return {
        'Factory': Factory,
        'Configs': ConfigsHolder,
        'AjaxForms': AjaxFormsHolder
    }


def get_user_likes(phrase_owner, like_owner):
    lst = LikedPhrases.objects.filter(phrase_owner=phrase_owner, like_owner=like_owner)
    likes = []
    for l in lst:
        obj = History.objects.get(id=l.phrase.id)
        likes.append(obj.id)
    print("oooooooooooooooooooooooooooooooooooooooooooooo - ", likes)
    return likes