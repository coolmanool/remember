from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render

from src.db.model_user import User
from src.db.model_history import History
from src.db.model_added import AddedWords
from src.db.model_liked_phrases import LikedPhrases
from src.db.model_statistics import Statistics
from src.factory import Factory

from .utils import build_context, get_user_likes


def hello(request):
    return render(request, 'hello.html', build_context())


def user(request, user_id):
    try:
        owner = User.objects.get(id=user_id)
    except User.DoesNotExist:
        raise Http404

    if request.user.is_authenticated():
        user_self_link = Factory.URLBuilder.build_user_page_url(request.user.id)
        likes = LikedPhrases.objects.filter(like_owner=request.user, phrase_owner=owner)
        likes_ids = [x.phrase_id for x in likes]
    else:
        likes_ids = []
        user_self_link = '/'

    phrases = History.objects.filter(user=owner.id)
    for p in phrases:
        p.has_like = p.id in likes_ids

    context = build_context()
    context.update({
        'owner': owner,
        'queries': phrases,
        'added': AddedWords.objects.filter(user=owner.id),
        'user_self': user_self_link
    })
    return render(request, 'user.html', context)


def main(request):
    context = build_context()
    if request.user.is_authenticated():
        context.update({
            'user_self': Factory.URLBuilder.build_user_page_url(request.user.id)
        })
    return render(request, 'main.html', context)


def statistics(request):
    context = build_context()

    date_signin = []
    count_signin = []
    for obj in Statistics.objects.filter(key='count_signin'):
        date_signin.append(str(obj.date))
        count_signin.append(obj.counter)

    date_queries = []
    count_queries = []
    for obj in Statistics.objects.filter(key='count_queries'):
        date_queries.append(str(obj.date))
        count_queries.append(obj.counter)

    date_add = []
    count_add = []
    for obj in Statistics.objects.filter(key='count_add'):
        date_add.append(str(obj.date))
        count_add.append(obj.counter)

    context.update({
        'date_signin': date_signin,
        'count_signin': count_signin,
        'date_queries': date_queries,
        'count_queries': count_queries,
        'date_add': date_add,
        'count_add': count_add
    })
    return render(request, 'statistics.html', context)


def test(request):
    return render(request, 'test.html', build_context())
