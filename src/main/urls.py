from django.conf.urls import url, include

from django.contrib import admin
admin.autodiscover()

from src.factory import Factory
from . import views

urlpatterns = [
    url(Factory.URLBuilder.build_ajax_folder_pattern(), include('src.ajax.urls')),

    url(Factory.URLBuilder.build_main_page_pattern(), views.main),
    url(Factory.URLBuilder.build_user_page_pattern(), views.user),
    url(Factory.URLBuilder.build_hello_page_pattern(), views.hello),
    url(Factory.URLBuilder.build_statistics_page_pattern(), views.statistics),

    url(r'^test$', views.test)
]
