from django.db import models

from .model_word import Word


class WordsLinksWeight(models.Model):
    word1 = models.ForeignKey(Word, related_name='+')
    word2 = models.ForeignKey(Word, related_name='+')
    weight = models.IntegerField()

    class Meta(object):
        unique_together = (('word1', 'word2'),)
