from django.db import models

from .model_user import User
from .model_history import History


class LikedPhrases(models.Model):
    like_owner = models.ForeignKey(User, related_name='+')
    phrase_owner = models.ForeignKey(User, related_name='+')
    phrase = models.ForeignKey(History)

    class Meta(object):
        unique_together = (('like_owner', 'phrase'),)