import itertools
import operator
import random
from datetime import datetime
from django.db import IntegrityError
from django.db.models import F

from .model_word import Word
from .model_words_links_weight import WordsLinksWeight
from .model_added import AddedWords
from .model_liked_phrases import LikedPhrases
from src.conf.config import RedisConfig
from src.db.model_statistics import Statistics
from src.factory import Factory


class WordToNumberConverter(object):
    DICTIONARY_BASE = {
        'н': '0',
        'р': '1',
        'д': '2',
        'т': '3',
        'ч': '4',
        'п': '5',
        'ш': '6',
        'с': '7',
        'в': '8',
        'з': '9',
        'а': '',
        'ё': '',
        'у': '',
        'е': '',
        'о': '',
        'ы': '',
        'э': '',
        'я': '',
        'и': '',
        'ю': '',
        '-': ''
    }

    DICTIONARY_DOP = {
        'н': '0',
        'р': '1',
        'д': '2',
        'т': '3',
        'ч': '4',
        'п': '5',
        'ш': '6',
        'с': '7',
        'в': '8',
        'з': '9',
        'а': '',
        'ё': '',
        'у': '',
        'е': '',
        'о': '',
        'ы': '',
        'э': '',
        'я': '',
        'и': '',
        'ю': '',
        'й': '',
        'ц': '',
        'к': '',
        'л': '',
        'м': '',
        'х': '',
        'щ': '',
        'ъ': '',
        'ф': '',
        'ж': '',
        'б': '',
        'г': '',
        '-': '',
        'ь': ''
    }

    DICTIONARY_MIX = {
        'н': '0',
        'р': '1',
        'д': '2',
        'т': '3',
        'ч': '4',
        'п': '5',
        'ш': '6',
        'с': '7',
        'в': '8',
        'з': '9',
        'ф': '0',
        'ц': '1',
        'б': '2',
        'щ': '3',
        'к': '4',
        'г': '5',
        'л': '6',
        'ж': '7',
        'х': '8',
        'м': '9',
        'а': '',
        'ё': '',
        'у': '',
        'е': '',
        'о': '',
        'ы': '',
        'э': '',
        'я': '',
        'и': '',
        'ю': '',
        'й': '',
        'ъ': '',
        'ь': '',
        '-': ''
    }

    @classmethod
    def convert(cls, word):
        def __convert(dictionary):
            res = ''
            for ch in word:
                num = dictionary.get(ch, None)
                if num is not None:
                    res += num
                else:
                    res = ''
                    break
            return res

        res_mix = __convert(cls.DICTIONARY_MIX)
        res_base = __convert(cls.DICTIONARY_BASE)
        res_dop = __convert(cls.DICTIONARY_DOP)

        return res_base, res_mix, res_dop


def __update_words_weight(left, right):
    for word_left in left:
        for word_right in right:
            weight = Factory.DBCacheWordsLinks.get_weight(word_left, word_right)
            word_left.weight += weight
            word_right.weight += weight


def __update_words_weight_v2(left, right):
    right_ids = set(x.id for x in right)
    for word_left in left:
        if word_left.has_weight():
            for link in WordsLinksWeight.objects.filter(word1=word_left):
                if link.word2_id in right_ids:
                    word_left.weight += link.weight
                    next((x for x in right if x.id == link.word2_id)).weight += link.weight
                    print('log %s' % link.weight)


def get_words_order_by_weight(numbers, words_type, added_words_flag):
    words = [list(Word.objects.filter(**{words_type: num})) for num in numbers]
    if added_words_flag:
        added_words = [list(AddedWords.objects.filter(**{words_type: num})) for num in numbers]
        for w, a in zip(words, added_words):
            w.extend(a)

    left_it, right_it = itertools.tee(words)
    next(right_it, None)

    for left, right in zip(left_it, right_it):
         __update_words_weight_v2(left, right)

    for w in words:
        w.sort(key=operator.attrgetter('weight'), reverse=True)

    return words


def __try_get_group_auto_split(num, words_type, added_words_flag, min_words_count):
    def __order():
        __vars = '1'*3 + '2'*5 + '3'*6 + '4'*3 + '5'*1
        __ordered_vars = []
        while len(__vars) > 0:
            __v = random.choice(__vars)
            __vars = __vars.replace(__v, '')
            __ordered_vars.append(int(__v))
        return [__v for __v in __ordered_vars if __v <= len(num)]

    max_group = None
    max_group_words = []

    for count in __order():
        group = num[:count]
        group_words = get_words_order_by_weight([group], words_type, added_words_flag)[0]
        if len(group_words) >= min_words_count:
            return group, group_words
        if len(group_words) > len(max_group_words):
            max_group = group
            max_group_words = group_words

    return max_group, max_group_words


def __try_get_words_auto_split(num, words_type, added_words_flag, min_words_count):
    split_nums = []
    words = []

    while len(num) > 0:
        group, group_words = __try_get_group_auto_split(num, words_type, added_words_flag, min_words_count)
        if group is None:
            return None, None, 0

        split_nums.append(group)
        words.append(group_words)
        num = num[len(group):]

    return split_nums, words, min([len(w) for w in words])


def get_words_auto_split(num, words_type, added_words_flag, min_words_count, retry_count):
    good_split_nums, good_words, good_weight = None, None, 0

    for i in range(retry_count):
        split_nums, words, weight = __try_get_words_auto_split(num, words_type, added_words_flag, min_words_count)
        if weight >= min_words_count:
            return split_nums, words
        if weight > good_weight:
            good_split_nums = split_nums
            good_words = words
            good_weight = weight

    return good_split_nums, good_words


def statistics(key):
    statistic_upd = Statistics.objects.filter(date=datetime.today(), key=key).update(counter=F('counter')+1)
    if statistic_upd == 0:
        Statistics.objects.create(date=datetime.today(), key=key)