from django.db import models


class Statistics(models.Model):
    date = models.DateField()
    key = models.CharField(max_length=255)
    counter = models.IntegerField(default=1)

    class Meta(object):
        unique_together = (('date', 'key'),)