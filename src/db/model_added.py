from django.db import models

from .model_user import User


class AddedWords(models.Model):
    user = models.ForeignKey(User)
    word = models.CharField(max_length=50)
    num_base = models.CharField(max_length=25)
    num_mix = models.CharField(max_length=25)
    num_dop = models.CharField(max_length=25)
    weight = models.IntegerField(default=0)
    date = models.DateField()

    def __repr__(self):
        return self.word

    def to_json(self):
        return {
            'w': self.word,
            'i': -self.id
        }

    def has_weight(self):
        return False