from django.db import models

from .model_user import User


class History(models.Model):
    user = models.ForeignKey(User)
    query = models.CharField(max_length=50)
    words = models.CharField(max_length=100)
    likes = models.IntegerField(default=0)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.has_like = False

    def get_like_html_class(self):
        return 'badge-like' if self.has_like else ''

    class Meta(object):
        unique_together = (('user', 'query', 'words'),)
