from django.db import models

from src.conf.config import RedisConfig
from src.factory import Factory


# noinspection PyMethodMayBeStatic
class User(models.Model):
    signup_date = models.DateField(auto_now_add=True)
    is_superuser = models.BooleanField(default=False)
    vk_id = models.IntegerField(unique=True)
    name = models.CharField(max_length=50)


    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(User, self).save(force_insert, force_update, using, update_fields)
        Factory.DBCache.update_object(self, {'id': self.id}, RedisConfig.DB_CACHE_USER_EXPIRE)

    def is_authenticated(self):
        return True


# noinspection PyMethodMayBeStatic
class AnonymousUser(object):
    def is_authenticated(self):
        return False
